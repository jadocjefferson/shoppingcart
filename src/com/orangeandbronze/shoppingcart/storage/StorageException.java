package com.orangeandbronze.shoppingcart.storage;

import java.io.FileNotFoundException;
import java.io.IOException;

public class StorageException extends RuntimeException {

    public StorageException(Exception e){
        super(e.getMessage());
    }

    public StorageException(FileNotFoundException e, String FileName){
        super(FileName+" FILE NOT FOUND");
    }

    public StorageException(IOException e){
        super(e.getMessage());
    }

}
