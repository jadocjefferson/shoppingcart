package com.orangeandbronze.shoppingcart.storage;

import com.orangeandbronze.shoppingcart.domain.Cart;
import com.orangeandbronze.shoppingcart.domain.Product;

import java.io.*;

/** Stores Cart data in files. **/
public class Repository {

	/**
	 * Saves the contents of the cart in a file named "Cart" plus the cart ID.
	 * For example, if the Cart ID is 4, the filename is "Cart4".
	 **/
	public void saveCart(Cart cart) {
		// TODO Implement this method.
		String filename = "Cart"+cart.getCartId();
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(filename+".txt"));
			writer.write(cart.toString());
			writer.close();
		} catch (IOException e) {
			throw new StorageException(e);
		}
	}

	/**
	 * Retrieves a Cart object given the ID. Looks for the data in a file named
	 * "Cart" plus the Cart ID. For example, if the Cart ID is 4, the filename
	 * is "Cart4".
	 */
	public Cart findCartById(int id) {
		// TODO Implement this method.
		String filename = "Cart"+id;
		try {

			BufferedReader cartReader = new BufferedReader(new FileReader(filename+".txt"));
			String content = cartReader.readLine();
			String contextConverted = content.substring(content.indexOf("{")+1, content.indexOf("}"));
			String[] toArray = contextConverted.split(",");

			Cart cart = new Cart(id);// Create cart object
			for (String data:
					toArray) {
				//PARAMETER (PRODUCT,QTY) 0,1
				//Split by equal sign to separate
				//cart.add(data.split("=")[0],data.split("=")[0]);
				//System.out.println("PRODUCT:"+Product.valueOf(data.split("=")[0]));
				String product =data.split("=")[0].replaceAll("\\s+","");
                cart.add(Product.valueOf(product),
						Integer.parseInt(data.split("=")[1]));
			}
			return cart;
		} catch (FileNotFoundException e) {
			throw new StorageException(e, filename);
		} catch (IOException e) {
			throw new StorageException(e);
		}
	}


}
