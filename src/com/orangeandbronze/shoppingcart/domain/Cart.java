package com.orangeandbronze.shoppingcart.domain;

import java.security.Key;
import java.util.*;

public class Cart {
	private int cartno;
	//Product, Quantity
	HashMap<Product,Integer> listOfproduct;

	public Cart(int num){
		this.cartno = num;
		listOfproduct = new HashMap<>();
	}

	public int getCartId() {
		// TODO Implement this method.
		return cartno;
	}

	/** Gets all the products in the cart, but not the quantities. **/
	public Collection<Product> getProducts() {
		// TODO Implement this method.
		List<Product> list = new ArrayList<Product>(this.listOfproduct.keySet());
		return list;
	}

	/**
	 * Adds a product item to the cart. If the product is not yet in the card,
	 * this method adds the product with quantity 1. If the product is already
	 * in the cart, this method will increment the product quantity by 1.
	 */
	public void add(Product product) {
		// TODO Implement this method.
		//CHECK IF PRODUCT is exist\
		//System.out.println(listOfproduct.containsKey(product));
		if (!listOfproduct.containsKey(product)){
			this.listOfproduct.put(product,1);
		}else{
			for ( Map.Entry<Product, Integer> entry : this.listOfproduct.entrySet()) {
				Product key = entry.getKey();
				Integer val = entry.getValue();
				// do something with key and/or tab
				//System.out.println("KEY: "+key.equals(product));
				if(key.equals(product)){
					this.listOfproduct.put(product,val+1);
				}
			}

		}
	}

	/**
	 * Adds product items to the cart. If the product is not yet in the card,
	 * this method adds the product with the quantity given. If the product is
	 * already in the cart, this method will increment the current product
	 * quantity by the quantity given. If the quantity given is negative, throws
	 * IllegalArgumentException.
	 */
	public void add(Product product, int qty) {
		// TODO Implement this method.
		if (!listOfproduct.containsKey(product)){
			this.listOfproduct.put(product,qty);
		}else{
			for ( Map.Entry<Product, Integer> entry : this.listOfproduct.entrySet()) {
				Product key = entry.getKey();
				Integer val = entry.getValue();
				// do something with key and/or tab
				//System.out.println("KEY: "+key.equals(product));
				if(key.equals(product)){
					this.listOfproduct.put(product,val+qty);
				}
			}

		}
	}

	/**
	 * Returns quantity of the product in the cart or zero if the product is not
	 * in the cart.
	 **/
	public int getQuantity(Product product) {
		// TODO Implement this method.
		if (listOfproduct.containsKey(product)){
			for ( Map.Entry<Product, Integer> entry : this.listOfproduct.entrySet()) {
				Product key = entry.getKey();
				Integer val = entry.getValue();
				// do something with key and/or tab
				//System.out.println("KEY: "+key.equals(product));
				if (key.equals(product)) {
					//System.out.println(val);
					return val;
				}
			}
		}
		return 0;
	}

	/**
	 * Removes product from the cart. Equivalent to setting quantity to zero.
	 **/
	public void delete(Product product) {
		// TODO Implement this method.

		this.listOfproduct.remove(product);
	}

	/**
	 * Sets the quantity of the product in the cart.
	 * - If the product is already in the cart, the old quantity is discarded.
	 * - If the quantity is zero, the product is deleted from the cart.
	 * - If the product is not yet in the cart, the product is added with the quantity.
	 * - If quantity is negative, throws IllegalArgumentException.
	 */
	public void setQuantity(Product product, int qty) {
		// TODO Implement this method.
		this.listOfproduct.put(product,qty);
	}

	@Override
	public String toString(){
		return "Cart #"+this.cartno+" "+this.listOfproduct;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return ((Cart) obj).getCartId() == this.getCartId();
	}
}
